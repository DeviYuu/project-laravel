<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=>, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1> Buat Account Baru!</h1>
    <h2> Sign Up Form </h2>
    <form action="/welcome" method="post">
        @csrf
        <label> First Name: </label> <br><br>
        <input type="text" name="nama"> <br><br>
        <label> Last Name: </label> <br><br>
        <input type="text" name="nama"> <br><br>
        <label> Gender: </label> <br><br>
        <input type="radio" name="gender" >Male <br>
        <input type="radio" name="gender" >Female <br>
        <input type="radio" name="gender" >Other <br><br>
        <label> Nationality: </label> <br><br>
        <select name="nationality">
            <option value="Indonesian"> Indonesian </option>
            <option value="Singaporean"> Singaporean </option>
            <option value="Malaysian"> Malaysian </option>
            <option value="Australian"> Australian </option>
        </select> <br><br>
        <label> Language Spoken: </label> <br><br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br><br>
        
        <label> Bio: </label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br><br>
        <input type="submit" name="submit" value="Sign Up">
    </form>
</body>
</html>